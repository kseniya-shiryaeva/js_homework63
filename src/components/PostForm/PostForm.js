import React from 'react';
import './PostForm.css';

const PostForm = ({post, onInputChange, createNewPost}) => {
    return (
        <form onSubmit={createNewPost} className="PostForm">
            <input
                className="Input"
                type="hidden"
                name="createDate"
                value={post.createDate}
                onChange={onInputChange}
            />
            <label htmlFor="title">Title</label>
            <input
                className="Input"
                type="text"
                name="title"
                placeholder="Post title"
                value={post.title}
                onChange={onInputChange}
            />
            <label htmlFor="description">Description</label>
            <textarea
                className="Textarea"
                name="description"
                placeholder="Post description"
                value={post.description}
                onChange={onInputChange}>{post.description}</textarea>
            <button type="submit">SAVE</button>
        </form>
    );
};

export default PostForm;