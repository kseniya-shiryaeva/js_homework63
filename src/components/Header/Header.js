import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
import SiteMenu from "../SiteMenu/SiteMenu";

const Header = () => {
    return (
        <div className="Header">
            <NavLink to="/"><span className="logo">My Blog</span></NavLink>
            <SiteMenu />
        </div>
    );
};

export default Header;