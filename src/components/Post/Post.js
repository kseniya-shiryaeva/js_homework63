import React from 'react';
import {NavLink} from "react-router-dom";
import './Post.css';

const Post = ({createDate, title, id}) => {
    const postUrl = "/posts/" + id;

    return (
        <div className="Post">
            <p className="createDate">Created on: {createDate}</p>
            <h4>{title}</h4>
            <NavLink to={postUrl}>Read more >></NavLink>
        </div>
    );
};

export default Post;