import React from 'react';
import './SiteMenu.css';
import {NavLink} from "react-router-dom";

const SiteMenu = () => {
    return (
        <div className="SiteMenu">
            <NavLink to="/">Home</NavLink>
            <NavLink to="/posts/add">Add</NavLink>
            <NavLink to="/page/about">About</NavLink>
            <NavLink to="/page/contacts">Contacts</NavLink>
        </div>
    );
};

export default SiteMenu;