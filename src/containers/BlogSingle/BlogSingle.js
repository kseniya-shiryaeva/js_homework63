import React, {useEffect, useState} from 'react';
import './BlogSingle.css';
import axiosApi from "../../axiosApi";
import {NavLink} from "react-router-dom";

const BlogSingle = ({match, history}) => {
    const [post, setPost] = useState(null);

    useEffect(() => {
        const getText = async () => {
            const request = await axiosApi('/posts/' + match.params.id + '.json');
            setPost(request.data);
        };
        getText();
    }, [match.params.id]);

    const RemovePost = async () => {
        try {
            await axiosApi.delete('posts/' + match.params.id + '.json');
        } finally {
            history.replace('/');
        }
    };

    let content = 'Текст недоступен';

    if (post) {
        const editUrl = "/posts/" + match.params.id + "/edit";

        content = (
            <>
                <h1>{post.title}</h1>
                <p className="createDate">{post.createDate}</p>
                <div>{post.description}</div>
                <div className="buttonBlock">
                    <NavLink to={editUrl} className="actionButton">Редактировать</NavLink>
                    <button onClick={RemovePost} className="actionButton">Удалить</button>
                </div>
            </>
        );
    }

    return (
        <div className="BlogSingle">
            {content}
        </div>
    );
};

export default BlogSingle;