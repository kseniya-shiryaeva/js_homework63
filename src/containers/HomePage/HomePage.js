import React, {useEffect, useState} from 'react';
import './HomePage.css';
import Post from "../../components/Post/Post";
import axiosApi from "../../axiosApi";

const HomePage = () => {
    const [posts, setPosts] = useState(null);

    useEffect(() => {
        const getPosts = async () => {
            const request = await axiosApi.get('/posts.json');
            console.log(request.data);
            setPosts(request.data);
        };
        getPosts();
    }, []);

    let content = 'Постов пока нет';

    if (posts) {
        content = Object.keys(posts).map(key => {
                return <Post key={key} title={posts[key].title} id={key} createDate={posts[key].createDate} />
            });
    }

    return (
        <div className="HomePage">
            {content}
        </div>
    );
};

export default HomePage;