import React, {useEffect, useState} from 'react';
import './TextPage.css';
import axiosApi from "../../axiosApi";

const TextPage = ({match}) => {
    const [page, setPage] = useState(null);

    useEffect(() => {
        const getText = async () => {
            const request = await axiosApi('/page/' + match.params.alias + '.json');
            setPage(request.data);
        };
        getText();
    }, [match.params.alias]);

    let content = 'Текст недоступен';

    if (page) {
        content = (
            <>
                <h1>{page.title}</h1>
                <div>{page.content}</div>
            </>
        );
    }

    return (
        <div className="TextPage">
            {content}
        </div>
    );
};

export default TextPage;