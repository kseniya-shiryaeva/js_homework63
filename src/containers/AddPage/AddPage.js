import React, {useState} from 'react';
import axiosApi from "../../axiosApi";
import dayjs from "dayjs";
import Spinner from "../../components/UI/Spinner/Spinner";
import PostForm from "../../components/PostForm/PostForm";

const AddPage = ({history}) => {
    const [loading, setLoading] = useState(false);

    const [post, setPost] = useState({
        title: '',
        description: '',
        createDate: dayjs().format('DD.MM.YYYY HH:mm:ss')
    });

    const createNewPost = async e => {
        e.preventDefault();
        setLoading(true);
        try {
            await axiosApi.post('posts.json', post);
        } finally {
            setLoading(false);
            history.replace('/');
        }
    }

    const onInputChange = e => {
        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    let form = (
        <PostForm post={post} onInputChange={onInputChange} createNewPost={createNewPost}/>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="AddPage">
            <h1>Add new post</h1>
            {form}
        </div>
    );
};

export default AddPage;