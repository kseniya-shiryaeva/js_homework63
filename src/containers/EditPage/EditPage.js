import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import PostForm from "../../components/PostForm/PostForm";
import Spinner from "../../components/UI/Spinner/Spinner";

const EditPage = ({history, match}) => {
    const [loading, setLoading] = useState(false);

    const [post, setPost] = useState({
        title: '',
        description: '',
        createDate: ''
    });

    useEffect(() => {
        const getPost = async () => {
            const request = await axiosApi('/posts/' + match.params.id + '.json');
            setPost(request.data);
        };
        getPost();
    }, [match.params.id]);


    const editPost = async e => {
        e.preventDefault();
        setLoading(true);
        try {
            await axiosApi.put('posts/' + match.params.id + '.json', post);
        } finally {
            setLoading(false);
            history.replace('/');
        }
    };

    const onInputChange = e => {
        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    let form = (
        <PostForm post={post} onInputChange={onInputChange} createNewPost={editPost}/>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="EditPage">
            <h1>Edit post</h1>
            {form}
        </div>
    );
};

export default EditPage;