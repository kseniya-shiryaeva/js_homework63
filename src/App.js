import './App.css';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import AddPage from "./containers/AddPage/AddPage";
import TextPage from "./containers/TextPage/TextPage";
import BlogSingle from "./containers/BlogSingle/BlogSingle";
import EditPage from "./containers/EditPage/EditPage";

function App() {
  return (
    <div className="App">
      <Header />
        <div className="content">
            <Switch>
                <Route path="/" exact component={HomePage} />
                <Route path="/posts" exact component={HomePage} />
                <Route path="/posts/add" component={AddPage} />
                <Route path="/page/:alias" component={TextPage} />
                <Route path="/posts/:id/edit" component={EditPage} />
                <Route path="/posts/:id" component={BlogSingle} />
                <Route render={()=><h1>404: Page is not found</h1>} />
            </Switch>
        </div>
    </div>
  );
}

export default App;
